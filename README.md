# WFC - Vagrant y ansible

## Descripcion

Este ejercicio levanta dos maquinas virtuales llamadas **default** y **ansible**
cuya diferencia es su forma de aprovisionamiento.

Ambas maquinas instalan dependencias, las cuales son:

* nodejs
* npm
* apache

Luego clonan el repositorio: https://gitlab.com/chudix/my-json-resume

Para finalmente crear el cv en formato html y servirlo por apache en localhost. La
maquina default lo sirve en el puerto 8008 y la maquina ansible en el puerto 8080.

## Set up

Para levantar las maquinas es necesario tener ansible instalado. A tal efecto se
proveen [Pipfile](./Pipfile) y [requirements.txt](./requirements.txt) para 
el manejo de esta dependencia en ambientes virtuales de python. De esta forma, con
el ambiente activado basta con ejecutar:

```sh
pip install -r requirements.txt

#o bien
pipenv install
```

## Ejecucion

En el directorio del proyecto basta con ejecutar:

```sh
vagrant up
```
para que se creen las maquinas virtuales aprovisionadas.

Una vez levantadas las VMs podemos verificar su funcionamiento dirigiendonos a
127.0.0.1:8080 y 127.0.0.1:8008 para verlas a ambas sirviendo el archivo del
CV.
