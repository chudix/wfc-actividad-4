#!/usr/bin/env bash

if [[ -d ~/my-json-resume ]]; then
  echo "Repo already exists."
  cd ~/my-json-resume
  git pull
else
  echo "No repo on vm. Clonning"
  git clone https://gitlab.com/chudix/my-json-resume.git
  cd ~/my-json-resume
fi;

npm i
cd
