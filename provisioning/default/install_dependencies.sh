#!/usr/bin/env bash

apt-get update
apt-get install -y nodejs=10.19.0* npm=6.14.4* apache2=2.4.41*
