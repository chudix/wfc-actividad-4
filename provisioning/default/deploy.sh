#!/usr/bin/env bash

cd /home/vagrant/my-json-resume
npx resume export /var/www/html/index.html --theme paper
